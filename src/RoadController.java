import core.*;
import core.Camera;

import java.util.Scanner;

public class RoadController {

    private static double passengerCarMaxWeight = 3500.0; // kg   //  Переменные типа double
    private static int passengerCarMaxHeight = 2000; // mm макс  //  Переменные типа int
    private static int controllerMaxHeight = 4000; // mm  //  Переменные типа int

    private static int passengerCarPrice = 100; // RUB  //  Переменные типа int
    private static int cargoCarPrice = 250; // RUB  //  Переменные типа int
    private static int vehicleAdditionalPrice = 200; // RUB д  //  Переменные типа int

    public static void main(String[] args) {
        System.out.println("Сколько автомобилей сгенерировать?");

        Scanner scanner = new Scanner(System.in); //Переменная типа  Scanner

        int carsCount = scanner.nextInt(); // Переменная типа int

        for (int i = 0; i < carsCount; i++) { // Переменная типа int ?
            Car car = Camera.getNextCar(); // Переменная типа Car
            System.out.println(car);

            //Пропускаем автомобили спецтранспорта бесплатно
            if (car.isSpecial) {
                openWay();
                continue;
            }

            //Проверяем высоту и массу автомобиля, вычисляем стоимость проезда
            int price = calculatePrice(car); // Переменная типа  int
            if (price == -1) {
                continue;
            }

            System.out.println("Общая сумма к оплате: " + price + " руб.");
        }
    }

    /**
     * Расчёт стоимости проезда исходя из массы и высоты
     */
    private static int calculatePrice(Car car) { //  Переменная типа Car
        int carHeight = car.height; //Переменная типа  int
        int price = 0; //Переменная типа  int
        if (carHeight > controllerMaxHeight) {   // Переменная типа int
            blockWay("высота вашего ТС превышает высоту пропускного пункта!");
            return -1; //  вернуть  -1
        } else if (carHeight > passengerCarMaxHeight) { // Переменная типа int
            double weight = car.weight; //Переменная типа  double
            //Грузовой автомобиль
            if (weight > passengerCarMaxWeight) {

                price = passengerCarPrice;
                if (car.hasVehicle) {
                    price = price + vehicleAdditionalPrice;
                }
            }
            //Легковой автомобиль
            else {
                price = cargoCarPrice;
            }
        } else {
            price = passengerCarPrice;
        }
        return price;
    }

    /**
     * Открытие шлагбаума
     */
    private static void openWay() {
        System.out.println("Шлагбаум открывается... Счастливого пути!");
    }

    /**
     * Сообщение о невозможности проезда
     */
    private static void blockWay(String reason) { // Переменная типа String
        System.out.println("Проезд невозможен: " + reason);
    }
}